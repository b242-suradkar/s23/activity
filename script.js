let trainer = {

		name : 'Ash Ketchum',
		age : 10,
		pokemon : ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
		friends : {
			kanto: ['Brock', 'Misty'],
			hoenn: ['May', 'Max']
		}
}

	trainer.talk = function() {
		console.log('Pikachu! I choose you!');
	}

	console.log(trainer);
	console.log('Result of dot notation:');
	console.log(trainer.name);
	console.log('Result of square bracket notation:');
	console.log(trainer['pokemon']);
	console.log('Result of talk method');
	trainer.talk();

function Pokemon(name,level) {

	
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = 3*level;

	
	this.tackle = function(target) {

	    console.log( this.name + " tackled " + target.name);
	    target.health = target.health - this.attack;
	    console.log( target.name +"'s health is now reduced to " + target.health);
	    
	 
	    if (target.health <= 0) {
	    	
	        target.faint()
	    }

	};

	this.faint = function(){
	    console.log( this.name +" fainted");
	}

}

let pikachu = new Pokemon("Pikachu",5);
console.log(pikachu);


let Ratata = new Pokemon("Ratata", 3);
console.log(Ratata);


let charmendor = new Pokemon("charmendor", 4);
console.log(charmendor);


charmendor.tackle(pikachu);
console.log(pikachu);


charmendor.tackle(Ratata);
console.log(Ratata);
